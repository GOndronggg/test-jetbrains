const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const mongoose = require("mongoose");
require("dotenv").config();
const { MONGOOSE } = process.env;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

try {
  // Connect to the MongoDB cluster
  mongoose.connect(
    MONGOOSE,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
    () => console.log(" Mongoose is connected")
  );
} catch (e) {
  console.log("could not connect");
}

const Schema = mongoose.Schema;
//Table
const userSchema = new Schema(
  {
    firstname: {
      type: String,
      minlength: 3,
      maxlength: 255,
    },
    lastname: {
      type: String,
      minlength: 3,
      maxlength: 255,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      match: [
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Please enter a valid email",
      ],
    },
    password: {
      type: String,
    },
    phone: {
      type: String,
      default: "none",
      minlength: 3,
      maxlength: 255,
    },
    address: {
      type: String,
      default: "none",
      minlength: 3,
      maxlength: 255,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    updatedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "users",
  }
);

const user = mongoose.model("User", userSchema);

/*
app.get("/users", async (req, res) => {
  const verifyToken = jwt.verify(req.headers.authorization, "secret_key");

  const user = await userSchema
    .findOne({ username: verifyToken.email })
    .populate({ path: "users" });

  res.send({
    status: 200,
    data: user,
  });
});
*/

try {
  app.post("/register", async (req, res) => {
    const payload = req.body;

    payload.salt = await bcrypt.genSalt(10);
    payload.password = await bcrypt.hash(payload.password, payload.salt);

    const newUser = new user(payload);
    const result = newUser.save();

    res.send({
      status: 200,
      data: result,
    });
  });
} catch (err) {
  throw new Error();
}

/*
app.post("/login", async (req, res) => {
  const payload = req.body;

  let message = "";
  const userExist = await userSchema.findOne({ email: payload.email });
  if (!userExist) {
    message = "user not found";
  }

  const isPasswordMatch = await bcrypt.compare(
    payload.password,
    userExist.password
  );
  if (!isPasswordMatch) {
    message = "invalid password";
    res.send(message);
  } else {
    const token = await jwt.sign(userExist.toJSON(), "secret_key");
    res.send({
      status: 200,
      message,
      data: {
        token,
      },
    });
  }
});
*/

app.listen(5000, console.log("app is running on port 5000"));
